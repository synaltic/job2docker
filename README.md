# Talend Docker

Ce project est basé sur : [Talend/job2docker](https://github.com/Talend/job2docker)

Ce référentiel comprend des exemples de scripts et utilitaires bash vous permettant de déployer des jobs Talend dans des conteneurs Docker.

L'approche **job2docker** convertit un seul fichier zip d'un job buildé Talend en un conteneur. L'image Docker résultante aura un seul point d'entrée pour le job. Il est destiné à être utilisé par les développeurs pendant leur cycle de construction / test / débogage et fournit une parité de bureau.

## Démarrage rapide

Clone répertoire 

```bash
git clone ssh://git@gitlab.app.synaltic.eu:30022/ycanario/job2docker.git
```

```bash
cd job2docker
```

Démarrer le bash de l'installation

```bash
./job2docker-setup
```

> Vous pouvez passer les deux arguments suivants à la fonction si besoin :
* $1 : repertoire_partagé. *Défaut : ${HOME}/shared_jobs*
* $2 : repertoire_du_traitement. *Défaut : ${HOME}/talend*

> exemple : 

> ./job2docker-setup /home/monutilisateur/jobs_buildes_partages /home/monutilisateur/job2docker_traitement

Une fois que l'installation est terminée, vous pouvez accéder au répertoire $2 (*défaut : ${HOME}/talend*) contenant les scripts bash, jobs et configurations nécessaires pour construire l'image Docker à partir d'un job buildé

```bash
cd ${HOME}/talend
```

Lancer le job2docker

```bash
./job2docker_listener
```



Job2docker est prêt à recevoir des jobs buildés. Ce programme va vérifier toutes les 10 secondes s'il y a un nouveau .zip dans le répertoire partagé $1 et va construire automatiquement l'image Docker. Les logs seront affichés dans la console.

## Exemple Hello World

1 . Importer le job *.../job2docker/jobs/docker_hello_world_0.1.zip* sur Talend

2 . Builder le job dans le repertoire_partagé $1. *La console qui lance job2docker doit afficher les resultat de la conversion* 

3 . Vérifier que l'image a été bien crééé : 

L'image aura comme nom : *NOM_DU_CLIENT (défaut $USER)* / *NOM_DU_PROJET* / *NOM_DU_JOB* : *VERSION_DU_JOB*

> $ docker images

> ```bash
REPOSITORY                                          TAG                 IMAGE ID            CREATED             SIZE
yabir/gameofthrones/recuperation_personnages_test   0.1                 4c56cd4bd004        5 seconds ago       176MB
synaltic/se_demo/docker_hello_world                 0.1                 ee4b1a0fb9ae        5 hours ago         179MB
synaltic/gameofthrones/hellojontargaryen            0.1                 6ad0e5b4d5c3        31 hours ago        176MB
```

4 . Lancer un container à partir de l'image

```bash
docker run NOM_OU_ID_DE_L_IMAGE
```

5 . Vous devriez voir une sortie similaire à celle ci-dessous :

> $ docker run ee4b1a0fb9ae 

> hello world